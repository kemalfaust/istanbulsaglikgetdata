package com.akademiteknoloji.istanbulsaglikgetdata;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;


import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {

    private WebView webView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        webView = new WebView(getApplicationContext());

        webView.getSettings().setJavaScriptEnabled(true);
        webView.addJavascriptInterface(new JsBridge(getApplicationContext()),"Android");

       this.getToken();




    }

    private void getToken(){

        webView.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);

                view.loadUrl("javascript:window.Android.htmlContentForToken('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");


            }
        });

        webView.loadUrl("http://apps.istanbulsaglik.gov.tr/Eczane");


    }

    private void getNobetciDetail(String token){

        webView.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);

                view.loadUrl("javascript:window.Android.htmlEczaneDetail('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");


            }
        });

        webView.loadUrl("http://apps.istanbulsaglik.gov.tr/Eczane/nobetci?id=3&token="+token);

    }

    private Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 1){
                String token = (String) msg.obj;
                getNobetciDetail(token);
            }


        }
    };


    class JsBridge {
        private Context context;
        public JsBridge(Context context) {
            this.context = context;
        }

        @JavascriptInterface
        public void htmlContentForToken(String str){

            String s[] = str.split("token");
            if (s.length>1){
                //"token": "2ad68bdc0856c429" },

                String s2[] = s[1].split(Pattern.quote("}"));
                String tkn = s2[0].replaceAll(" ","").replaceAll("\"","").replaceAll(":","");

                Log.i("TOKEN", tkn);
                Toast.makeText(context,"TOKEN "+tkn,Toast.LENGTH_SHORT).show();
                Message message = new Message();
                message.what = 1;
                message.obj = tkn;
                handler.sendMessage(message);


            }


        }

        @JavascriptInterface
        public void htmlEczaneDetail(String str){

            Log.i("htmlEczaneDetail", str);
            Toast.makeText(context,"htmlEczaneDetail \n "+str,Toast.LENGTH_LONG).show();


        }
    }


}
